package it.unibo.oop.lab.nesting2;

import java.util.*;

public class OneListAcceptable<T> implements Acceptable<T> {

	private final List<T> list;
		
	public OneListAcceptable(List<T> list) {
		this.list = list;
	}
	
	@Override
	public Acceptor<T> acceptor() {
		return new Acceptor<T>(){

			private int accepted = 0;
			private boolean allowToAccept = true;
			
			@Override
			public void accept(T newElement) throws ElementNotAcceptedException {
				if(!list.contains(newElement)) {
					throw new ElementNotAcceptedException(newElement);
				}
				if(isAllowedToAccept()) {
					this.accepted++;
				}
			}

			@Override
			public void end() throws EndNotAcceptedException {
				if(this.accepted != list.size()) {
					throw new EndNotAcceptedException();
				}
				if(isAllowedToAccept()) {
					this.allowToAccept = false; 
				}
			}
			
			private boolean isAllowedToAccept() {
				return this.allowToAccept;
			}
		};
	}

}
